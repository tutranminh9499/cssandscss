// var modal = document.getElementById("myModal");

// // Get the button that opens the modal
// var btn = document.getElementById("myBtn");

// // Get the <span> element that closes the modal
// var spa1n = document.getElementById("closeIcon");
// var btnSubmit = document.getElementById("btnSubmit");

// // When the user clicks on the button, open the modal
// btn.onclick = function () {
//   modal.style.display = "block";
//   console.log("abc");
// };

// // When the user clicks on <span> (x), close the modal
// spa1n.onclick = function () {
//   modal.style.display = "none";
// };
// btnSubmit.onclick = function () {
//   modal.style.display = "none";
//   insertData(
//     (id = newId),
//     (server = document.getElementById("server").value),
//     (requestFrom = document.getElementById("requestFrom").value),
//     (requestTo = document.getElementById("requestTo").value)
//   );
// };

// // When the user clicks anywhere outside of the modal, close it
// window.onclick = function (event) {
//   if (event.target == modal) {
//     modal.style.display = "none";
//   }
// };

var data = [];
var uniqueid = 100;

function newId() {
  return uniqueid++;
}
function initData() {
  for (let i = 1; i <= 4; i++) {
    data.push({
      id: i,
      status: "New",
      createdDate: new Date().toLocaleString(),
      createdBy: "John Brown",
      updatedDate: "",
      updatedBy: "John Brown",
      server: `apcoa-dk.giantleap.no - 46.255.17.${i}2`,
      // age: `${i}2`,
      // address: `New York No. ${i} Lake Park`,
      requestFrom: new Date().toLocaleString(),
      requestTo: new Date().toLocaleString(),
      title: `My name is John Brown`,
    });
  }
  for (let i = 5; i <= 7; i++) {
    data.push({
      id: i,
      status: "Open",
      createdDate: new Date().toLocaleString(),
      createdBy: "John Brown",
      updatedDate: "",
      updatedBy: "John Brown",
      server: `apcoa-dk.giantleap.no - 46.255.17.${i}2`,
      // address: `New York No. ${i} Lake Park`,
      requestFrom: new Date().toLocaleString(),
      requestTo: new Date().toLocaleString(),
      title: `My name is John Brown`,
    });
  }
  for (let i = 8; i <= 10; i++) {
    data.push({
      id: i,
      status: "Close",
      createdDate: new Date().toLocaleString(),
      createdBy: "John Brown",
      updatedDate: "",
      updatedBy: "John Brown",
      server: `apcoa-dk.giantleap.no - 46.255.17.${i}2`,
      // age: `${i}2`,
      // address: `New York No. ${i} Lake Park`,
      requestFrom: new Date().toLocaleString(),
      requestTo: new Date().toLocaleString(),
      title: `My name is John Brown`,
    });
  }
}

function initTable() {
  initData();

  for (x of data) {
    var table = document.getElementById("mytable");
    var row = table.insertRow(-1);

    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    var cell6 = row.insertCell(5);
    var cell7 = row.insertCell(6);
    var cell8 = row.insertCell(7);
    var cell9 = row.insertCell(8);
    var cell10 = row.insertCell(9);
    var cell11 = row.insertCell(10);
    var cell12 = row.insertCell(11);
    var cell13 = row.insertCell(12);
    cell1.innerHTML = `<td>
    <input type="checkbox" />
  </td>`;
    cell2.innerHTML = x.id;
    cell3.innerHTML = x.status;
    cell4.innerHTML = x.createdDate;
    cell5.innerHTML = x.createdBy;
    cell6.innerHTML = x.createdDate;
    cell7.innerHTML = x.updatedBy;
    cell8.innerHTML = x.server;
    cell9.innerHTML = x.requestFrom;
    cell10.innerHTML = x.requestTo;
    cell11.innerHTML = x.title;
    cell12.innerHTML = `
      <p data-placement="top" data-toggle="tooltip" title="Edit">
        <a
          class="button buttonEdit"
          data-title="Edit"
          data-toggle="modal"
          data-target="#edit"
        >
          Edit<span class="glyphicon glyphicon-pencil"></span>
        </a>
      </p>`;
    cell13.innerHTML = `
      <p>
        <a class="button buttonDetail" href="./requestDetail.html">
          Detail
        </a>
      </p>`;
  }
}

window.insertData = function (
  id = undefined,
  status = "New",
  createdDate = new Date().toLocaleString(),
  createdBy = "John Brown",
  updatedDate = new Date().toLocaleString(),
  updatedBy = "John Brown",
  server = "",
  requestFrom = undefined,
  requestTo = undefined,
  title = ""
) {
  var table = document.getElementById("mytable");
  var row = table.insertRow(-1);

  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  var cell3 = row.insertCell(2);
  var cell4 = row.insertCell(3);
  var cell5 = row.insertCell(4);
  var cell6 = row.insertCell(5);
  var cell7 = row.insertCell(6);
  var cell8 = row.insertCell(7);
  var cell9 = row.insertCell(8);
  var cell10 = row.insertCell(9);
  var cell11 = row.insertCell(10);
  var cell12 = row.insertCell(11);
  var cell13 = row.insertCell(12);
  cell1.innerHTML = `<td>
      <input type="checkbox" />
    </td>`;
  cell2.innerHTML = id;
  cell3.innerHTML = status;
  cell4.innerHTML = createdDate;
  cell5.innerHTML = createdBy;
  cell6.innerHTML = createdDate;
  cell7.innerHTML = updatedBy;
  cell8.innerHTML = server;
  cell9.innerHTML = requestFrom;
  cell10.innerHTML = requestTo;
  cell11.innerHTML = title;
  cell12.innerHTML = `
  <p data-placement="top" data-toggle="tooltip" title="Edit">
  <a
    class="button buttonEdit"
    data-title="Edit"
    data-toggle="modal"
    data-target="#edit"
  >
    Edit<span class="glyphicon glyphicon-pencil"></span>
  </a>
</p>`;
  cell13.innerHTML = `
  <p>
  <a class="button buttonDetail" href="./requestDetail.html">
    Detail
  </a>
</p>`;

  data.push({
    id: id,
    status: status,
    createdDate: createdDate,
    createdBy: createdBy,
    updatedDate: updatedDate,
    updatedBy: updatedBy,
    server: server,
    // address: `New York No. ${i} Lake Park`,
    requestFrom: requestFrom,
    requestTo: requestTo,
    title: title,
  });
};
