var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var spa1n = document.getElementById("closeIcon");
var btnSubmit = document.getElementById("btnSubmit");

// When the user clicks on the button, open the modal
btn.onclick = function () {
  modal.style.display = "block";
};

// When the user clicks on <span> (x), close the modal
spa1n.onclick = function () {
  modal.style.display = "none";
};
btnSubmit.onclick = function () {
  modal.style.display = "none";
  window.insertData(
    (id = newId()),
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    (server = document.getElementById("server").value),
    (requestFrom = document.getElementById("requestFrom").value),
    (requestTo = document.getElementById("requestTo").value),
    (title = document.getElementById("title").value)
  );
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
};
