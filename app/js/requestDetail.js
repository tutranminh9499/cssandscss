var commentId = "comment_";
var replyButtonId = "new-reply-";
var replyContainerId = "reply-";
var tempCommentBoxId = "temp-typebox-";
var mainCommentBoxId = "comment-box";
var comment_count = 6;

var comments = [
  {
    id: "comment_1",
    requestId: 1,
    commentId: null,
    content: "abcxyz",
    time: "15 min",
    avatar: "http://nicesnippets.com/demo/man01.png",
    username: "An",
    replies: [
      {
        id: "comment_2",
        requestId: 1,
        commentId: "comment_1",
        content: "abcxyz abcxyz",
        time: "15 min",
        avatar: "http://nicesnippets.com/demo/man01.png",
        username: "An",
      },

      {
        id: "comment_3",
        requestId: 1,
        commentId: "comment_1",
        content: "xxxxxxxx",
        time: "16 min",
        avatar: "http://nicesnippets.com/demo/man01.png",
        username: "An",
      },
    ],
  },

  {
    id: "comment_4",
    requestId: 1,
    commentId: null,
    content: "test reply 2",
    time: "19 min",
    avatar: "http://nicesnippets.com/demo/man01.png",
    username: "An",
    replies: [
      {
        id: "comment_5",
        requestId: 1,
        commentId: "comment_4",
        content: "yes ",
        timeStamp: "20 min",
        avatar: "http://nicesnippets.com/demo/man01.png",
        username: "B",
      },
    ],
  },
];

// Load comment and add event listener to typebox
function loadComment() {
  for (var i = 0; i < comments.length; i++) {
    addNewComment(
      comments[i].id,
      comments[i].avatar,
      comments[i].username,
      comments[i].content,
      comments[i].time,
      comments[i].replies
    );
  }
  // console.log(comments[0])
  // addNewComment(comments[0].id, comments[0].avatar, comments[0].username, comments[0].content, comments[0].time, [comments[1], comments[2]])

  var input = document.getElementById(mainCommentBoxId);
  input.addEventListener("keyup", function (event) {
    if (event.keyCode === 13) {
      var content = input.value;
      if (content) {
        console.log(content);
        addNewComment(
          commentId + comment_count,
          "http://nicesnippets.com/demo/man01.png",
          "xxxx",
          content,
          "0p",
          []
        );
        input.value = "";
      }
    }
  });
}

function addNewComment(id, avatar, username, content, time, replyList) {
  var comment = createNewComment(id, avatar, username, content, time);

  var comment = new Comment(id, avatar, username, content, time, replyList);
  comment.renderComment();
  // container.appendChild(comment);

  // var tempReply = new TempReply(false);
  // document.getElementById(replyButtonId + id).onclick = function() {
  //     tempReply.onClick(id);
  // }
}

function addNewReply(commentId, id, avatar, username, content, time) {
  var container = document.createElement("li");
  container.id = id;

  var comment = createNewComment(id, avatar, username, content, time);
  var mainComment = document.getElementById(replyContainerId + commentId);
  mainComment.appendChild(comment);
}

function replyComment(commentId) {
  var comment = document.getElementById(commentId);

  var container = document.createElement("div");
  container.classList.add("reply", "temp");

  var commentBoxContainer = document.createElement("div");
  commentBoxContainer.classList.add(
    "row",
    "comment-box-main",
    "p-0",
    "ml-2",
    "ml-2",
    "mr-2",
    "rounded-bottom"
  );

  var commentBox = document.createElement("div");
  commentBox.classList.add(
    "col-md-9",
    "col-sm-9",
    "col-9",
    "pr-0",
    "comment-box"
  );

  var input = document.createElement("input");
  input.classList.add("form-control");
  input.type = "text";
  input.placeholder = "comment .....";

  var sendButtonContainer = document.createElement("div");
  sendButtonContainer.classList.add(
    "col-md-3",
    "col-sm-2",
    "col-3",
    "pl-0",
    "text-center"
  );

  var sendButton = document.createElement("button");
  sendButton.classList.add("btn", "btn-info");
  sendButton.innerHTML = "Send";

  sendButtonContainer.appendChild(sendButton);
  commentBox.appendChild(input);

  commentBoxContainer.appendChild(commentBox);
  commentBoxContainer.appendChild(sendButton);

  container.appendChild(commentBoxContainer);

  comment.appendChild(container);
}

function createNewComment(id, avatar, username, content, time) {
  var container = document.createElement("ul");
  container.classList.add("p-0", "comment-container");
  container.id = id;
  container.innerHTML = `
        <div class="row main-comment comment mb-1">
            <div class="col-md-1 text-center user-img">
                <img id="profile-photo" src=${avatar} class="rounded-circle" />
            </div>
            <div class="col-md-11 comment-body rounded mb-2">
                <h4 class="m-0 comment-name"><b href="#"> ${username} </b></h4>
                <time class="ml-3 comment-timestamp">${time} </time>
                <p class="mb-0 comment-content"> ${content} </p>
                <button class="comment-reply-btn" id='${
                  replyButtonId + id
                }'>Reply</button>
            </div>
        </div>
        
        <ul id ="${replyContainerId + id}" class="reply">
        </ul>
        `;
  return container;
}

function renderReply(commentId, id, avatar, username, content, time) {
  var container = document.createElement("li");
  container.id = id;
  var comment = createNewComment(id, avatar, username, content, time);
  var conatiner = document.getElementById(replyContainerId + commentId);
  conatiner.appendChild(comment);
}

class Comment {
  constructor(id, avatar, username, content, timestamp, replies) {
    this.id = id;
    this.avatar = avatar;
    this.username = username;
    this.content = content;
    this.timestamp = timestamp;
    this.replies = replies;
  }

  renderComment() {
    var comment = createNewComment(
      this.id,
      this.avatar,
      this.username,
      this.content,
      this.timestamp
    );

    var container = document.getElementById("comments-container");

    container.appendChild(comment);

    var buttonId = replyButtonId + this.id;
    var containerId = this.id;
    var avatar = this.avatar;
    var username = this.username;

    var tempReply = new TempReply(false, containerId, username, avatar);

    document.getElementById(buttonId).onclick = function () {
      console.log(avatar);
      tempReply.onClick(containerId);
    };

    console.log(this.id);
    var children = this.replies;
    for (var i = 0; i < children.length; i++) {
      var reply = children[i];
      renderReply(
        containerId,
        reply.id,
        reply.avatar,
        reply.username,
        reply.content,
        reply.time
      );

      document.getElementById(replyButtonId + reply.id).onclick = function () {
        tempReply.onClick(containerId);
      };
    }
  }
}

class TempReply {
  constructor(clicked, id, username, avatar) {
    this.clicked = clicked;
    this.id = id;
    this.username = username;
    this.avatar = avatar;
  }

  createTempReply(commentId) {
    console.log(commentId);
    var comment = document.getElementById(commentId);
    console.log(this.avatar);
    var container = document.createElement("ul");
    container.classList.add("temp-reply-container");
    container.innerHTML = `
        <div class="row comment-box-main rounded-bottom">
            <div class="col-md-1 text-center user-img">
                <img id="profile-photo" src=${
                  this.avatar
                } class="rounded-circle" />
            </div>
            <div class="col-md-11 comment-box pl-0 pr-0">
                <input type="text" class="form-control" id=${
                  tempCommentBoxId + this.id
                } placeholder="comment ...." />
            </div>
        </div> `;

    comment.appendChild(container);
  }

  removeTempForm(commentId) {
    var comment = document.getElementById(commentId);
    comment.removeChild(comment.lastChild);
  }

  onClick(commentId) {
    var comment = document.getElementById(commentId);
    this.clicked = !this.clicked;
    if (this.clicked) {
      this.createTempReply(commentId);
      var id = this.id;
      var avatar = this.avatar;
      var username = this.username;
      var tempReply = this;
      var input = document.getElementById(tempCommentBoxId + id);
      input.addEventListener("keyup", function (event) {
        if (event.key === "Enter") {
          var content = input.value;
          if (content) {
            console.log(content);
            renderReply(commentId, id, avatar, username, content, "0p");

            document.getElementById(replyButtonId + id).onclick = function () {
              tempReply.onClick(containerId);
            };
            input.value = "";
          }
        }
      });
    } else {
      this.removeTempForm(commentId);
    }
  }
}
