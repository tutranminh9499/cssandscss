const email = document.getElementById("inputEmail");
const emailPattern = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
email.addEventListener("input", function (event) {
  console.log(emailPattern.test(email.value));
  if (!emailPattern.test(email.value)) {
    email.setCustomValidity("Incorrect email format!!");
  } else {
    email.setCustomValidity("");
  }
});
