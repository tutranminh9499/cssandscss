// var gulp = require("gulp");
// // Requires the gulp-sass plugin
// var sass = require("gulp-sass");
// var browserSync = require("browser-sync").create();
// // gulp.task("watch", function () {
// //   gulp.watch("app/css/*.css", gulp.series(["sass"]));
// //   // gulp.watch("app/js/*.js", gulp.series("scripts"));
// //   // gulp.watch("app/img/*", gulp.series("images"));
// // });
// // gulp.task("watch", gulp.series(["bSync", "sass"]), function () {
// //   gulp.watch("app/scss/**/*.scss", gulp.series(["sass"]));
// //   // Other watchers
// // });
// gulp.task("sass", function () {
//   return gulp
//     .src("app/scss/styles.scss")
//     .pipe(sass()) // Converts Sass to CSS with gulp-sass
//     .pipe(gulp.dest("app/css"));
// });
// // gulp.task("bsync", function () {
// //   browserSync.init({
// //     server: {
// //       baseDir: "app",
// //     },
// //   });
// // });
// // gulp.task("sass", function () {
// //   return gulp
// //     .src("app/scss/**/*.scss") // Gets all files ending with .scss in app/scss
// //     .pipe(sass())
// //     .pipe(gulp.dest("app/css"))
// //     .pipe(
// //       browserSync.reload({
// //         stream: true,
// //       })
// //     );
// // });

var gulp = require("gulp");
var browserSync = require("browser-sync").create();
var sass = require("gulp-sass");

// Compile sass into CSS & auto-inject into browsers
gulp.task("sass", function () {
  return gulp
    .src("app/scss/*.scss")
    .pipe(sass())
    .pipe(gulp.dest("app/css"))
    .pipe(browserSync.stream());
});

// Static Server + watching scss/html files
gulp.task(
  "serve",
  gulp.series("sass", function () {
    browserSync.init({
      server: "./app/",
    });

    gulp.watch("app/scss/*.scss", gulp.series("sass"));
    gulp.watch("app/*.html").on("change", browserSync.reload);
    gulp.watch("app/js/**/*.js").on("change", browserSync.reload);
  })
);

gulp.task("default", gulp.series("serve"));
